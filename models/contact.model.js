const mongoose = require('mongoose');

var contactSchema = new mongoose.Schema({
    Name: {
        type: String,
        required: 'This field is required.'
    },
    Email: {
        type: String
    },
    Mobile: {
        type: String,
        required: 'this fiels is required.'
    },
    
});

contactSchema.path('Email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');

mongoose.model('Contact', contactSchema);